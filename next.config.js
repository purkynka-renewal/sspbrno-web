/* eslint-disable @typescript-eslint/no-var-requires */
const { i18n } = require("./next-i18next.config");
const preactWebpack = require("./next.preact.config");

const withPreact = require("next-plugin-preact");

/**
 * @type {import('next').NextConfig}
 */
const config = {
  i18n,
  poweredByHeader: false,
  reactStrictMode: true,
  trailingSlash: true,
  compress: false, // NGINX should do this
  basePath: process.env.NEXT_PUBLIC_BASE_PATH || "",
  webpack: (_config, env) => {
    _config = preactWebpack(_config, env);
    return _config;
  },
  eslint: {
    ignoreDuringBuilds: true,
  },
  experimental: {
    modern: true,
    optimizeCss: true,
    optimizeImages: true,
    polyfillsOptimization: true,
    workerThreads: true,
  },
};

module.exports = withPreact(config);
